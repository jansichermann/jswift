import UIKit

protocol JSTableViewSectionProtocol {
    var numberOfRows: Int { get };
    var rows: [JSTableViewRowProtocol] { get };
    var title: String { get };
    
    func modelForRow(row: Int) -> JSTableViewRowProtocol;
}

extension JSTableViewSectionProtocol {
    func modelForRow(row: Int) -> JSTableViewRowProtocol {
        return rows[row];
    }
}

class JSTableViewSection: JSTableViewSectionProtocol {
    let numberOfRows: Int;
    let title: String
    let rows: [JSTableViewRowProtocol];
    
    init(rows:[JSTableViewRowProtocol], title: String) {
        self.rows = rows;
        self.title = title;
        numberOfRows = rows.count;
    }
}

protocol JSTableViewCellProtocol {
    typealias U
    func configure(model: U);
    static func heightForWidth(width: CGFloat, model: U) -> CGFloat;
}

protocol JSTableViewRowProtocol {
    var cellIdentifier: String { get }
    var backgroundColor: UIColor { get }
    var selectionStyle: UITableViewCellSelectionStyle { get }
    var editingStyle: UITableViewCellEditingStyle { get }
    var accessoryType: UITableViewCellAccessoryType { get }
    
    
    func configuredCell(tableView: UITableView) -> UITableViewCell;
    func heightForWidth(width: CGFloat) -> CGFloat;
    func didSelect();
    func didEdit();
}

class JSTableViewRowModel<T where T: UITableViewCell, T: JSTableViewCellProtocol>: JSTableViewRowProtocol {
    let model: T.U;
    let cellIdentifier = NSStringFromClass(T)
    let backgroundColor: UIColor;
    let selectionStyle: UITableViewCellSelectionStyle;
    let editingStyle: UITableViewCellEditingStyle;
    let accessoryType: UITableViewCellAccessoryType;
    
    private var onSelect: (()->())?
    private var onEdit: (()->())?
    
    init(model: T.U,
        backgroundColor: UIColor,
        selectionStyle: UITableViewCellSelectionStyle,
        editingStyle: UITableViewCellEditingStyle,
        accessoryType: UITableViewCellAccessoryType,
        onEdit: (()->())?,
        onSelect: (()->())?) {
            self.model = model;
            self.backgroundColor = backgroundColor;
            self.selectionStyle = selectionStyle;
            self.editingStyle = editingStyle;
            self.accessoryType = accessoryType;
            self.onEdit = onEdit;
            self.onSelect = onSelect;
    }
    
    convenience init(model: T.U, backgroundColor: UIColor) {
        self.init(model: model,
            backgroundColor: backgroundColor,
            selectionStyle: .Default,
            editingStyle: UITableViewCellEditingStyle.None,
            accessoryType: UITableViewCellAccessoryType.None,
            onEdit: nil,
            onSelect: nil)
    }
    
    convenience init(model: T.U, backgroundColor: UIColor, onSelect: (()->())?) {
        self.init(model: model,
            backgroundColor: backgroundColor,
            selectionStyle: .Default,
            editingStyle: UITableViewCellEditingStyle.None,
            accessoryType: UITableViewCellAccessoryType.None,
            onEdit: nil,
            onSelect: onSelect)
    }
    
    func configuredCell(tableView: UITableView) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? T;
        if cell == nil {
            cell = T(style: .Default, reuseIdentifier: cellIdentifier);
        }
        let c = cell!
        
        c.configure(self.model);
        c.backgroundColor = backgroundColor;
        c.selectionStyle = selectionStyle;
        c.accessoryType = accessoryType;
        return c;
    }
    
    func heightForWidth(width: CGFloat) -> CGFloat {
        return T.heightForWidth(width, model: self.model);
    }
    
    func didSelect() {
        if let os = self.onSelect {
            os();
        }
    };
    
    func didEdit() {
        if let de = self.onEdit {
            de();
        }
    }
}

class JSTableViewDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var sections = [JSTableViewSectionProtocol]()
    
    func modelForIndexPath(indexPath: NSIndexPath) -> JSTableViewRowProtocol {
        return sections[indexPath.section].modelForRow(indexPath.row);
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return modelForIndexPath(indexPath).heightForWidth(tableView.frame.width);
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        modelForIndexPath(indexPath).didSelect();
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return sections.count;
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].numberOfRows;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return modelForIndexPath(indexPath).configuredCell(tableView);
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return modelForIndexPath(indexPath).editingStyle != .None;
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        return modelForIndexPath(indexPath).editingStyle;
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        modelForIndexPath(indexPath).didEdit();
    }
    
    
    func tableView(tableView: UITableView, moveRowAtIndexPath sourceIndexPath: NSIndexPath, toIndexPath destinationIndexPath: NSIndexPath) { }
    
}
