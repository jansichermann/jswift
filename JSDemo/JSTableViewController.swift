import UIKit
import Foundation


class JSTableViewController: JSViewController {
    let tableView = UITableView();
    private let ds = JSTableViewDataSource();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        tableView.delegate = ds;
        tableView.dataSource = ds;
        tableView.separatorStyle = .None;
        view.addSubview(tableView);
        

        var rows = [JSTableViewRowProtocol]();
        rows.append(JSTableViewRowModel<TestCell>(model: "hi", backgroundColor: UIColor.whiteColor(), selectionStyle: .None, editingStyle:.None, accessoryType: .None, onEdit: nil, onSelect: nil));
        rows.append(JSTableViewRowModel<SpacingCell>(model: SpacingModel(height:1), backgroundColor: UIColor.blackColor()));
        
        ds.sections = [JSTableViewSectionProtocol]()
        ds.sections.append(JSTableViewSection(rows:rows, title: "bar"))
        
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        tableView.frame = view.bounds;
    }
}
