import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds);
        
        window?.rootViewController = JSTableViewController();
        window?.rootViewController?.view.backgroundColor = UIColor.redColor();
        window?.makeKeyAndVisible();
        return true
    }

}

