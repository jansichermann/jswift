import UIKit


class TestCell: UITableViewCell, JSTableViewCellProtocol {
    class func heightForWidth(width: CGFloat, model: String) -> CGFloat {
        return 64.0;
    }
    
    func configure(model: String) {
        self.textLabel?.text = model;
    }
}

struct SpacingModel {
    let height: CGFloat;
}

class SpacingCell: UITableViewCell, JSTableViewCellProtocol {
    class func heightForWidth(width: CGFloat, model: SpacingModel) -> CGFloat {
        return model.height;
    }
    
    func configure(model: SpacingModel) {}
}
